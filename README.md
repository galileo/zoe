# zoe

utilisation de l'api renault pour obtenir des informations sur mon véhicule


## Projet github écris en python pour l'utilisation de l'api
https://github.com/jamesremuscat/pyze

## URL pour obtenir les clés API
https://renault-wrd-prod-1-euw1-myrapp-one.s3-eu-west-1.amazonaws.com/configuration/android/config_fr_FR.json

ça marche enfin en utilisant les bonnes clés

```
curl https://renault-wrd-prod-1-euw1-myrapp-one.s3-eu-west-1.amazonaws.com/configuration/android/config_fr_FR.json |grep Prod -A2
		"wiredProd": {
			"target": "https://api-wired-prod-1-euw1.wrd-aws.com",
			"apikey": "oF09WnKqvBDcr....."
--
		"gigyaProd": {
			"target": "https://accounts.eu1.gigya.com",
			"apikey": "3_e8d4g4SE_Fo8ahyH......................................."
```


```
export KAMEREON_API_KEY="oF09WnKqvBDcr....."
export GIGYA_API_KEY="3_e8d4g4SE_Fo8ahyH......................................."
```




```
 pyze charge-history
Charge start         Charge end           Duration      Power (kW)    Charge gained (%)  Status
-------------------  -------------------  ----------  ------------  -------------------  --------
2019-10-16 19:54:53  2019-10-17 00:25:00  4:30:00              1.8                   29  ok
2019-10-15 20:00:39  2019-10-15 22:23:00  2:22:00              1.7                   13  ok
2019-10-15 19:55:21  2019-10-15 19:56:12  0:00:00              1.8                    0  ok
2019-10-14 17:11:19  2019-10-14 23:24:38  6:13:00              2                     41  ok
2019-10-14 16:18:47  2019-10-14 17:09:51  0:51:00              1.9                    8  ok
2019-10-14 14:54:43  2019-10-14 16:05:37  1:10:00              2                     12  ok
2019-10-14 13:49:47  2019-10-14 14:44:17  0:54:00              1.8                   10  ok
2019-10-14 13:47:23  2019-10-14 13:48:55  0:01:00              1.9                    1  ok
2019-10-14 13:24:08  2019-10-14 13:46:30  0:22:00              1.9                    4  ok
2019-10-14 13:22:38  2019-10-14 13:23:08  0:00:00              1.9                    0  ok
2019-10-14 13:21:14  2019-10-14 13:21:43  0:00:00              1.9                    0  ok
2019-10-14 13:10:17  2019-10-14 13:20:21  0:10:00              1.9                    2  ok
```

```
pyze charge-stats
Total time charging      Month    Number of charges    Errors
---------------------  -------  -------------------  --------
16:38:00                201910                   12         0
```


```
pyze status
--------------------  -------------------
Battery level         60%
Range estimate        40.4 miles
Plugged in            No
Charging              No
Charge mode           Always charge
AC state              off
External temperature  9.0°C
Total mileage         21213.0 mi
Updated at            2019-10-18 08:01:42
--------------------  -------------------
```
